import cv2
import numpy as np
#from gtts import gTTS
import os
import pygame


pygame.mixer.init()

#tts = gTTS(text='left', lang='en')
#tts.save("left.mp3")

#tts = gTTS(text='right', lang='en')
#tts.save("right.mp3")



face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
cap = cv2.VideoCapture(0)
font = cv2.FONT_HERSHEY_COMPLEX_SMALL
while 1:
    ret, img = cap.read()
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.8, 3)
    for (x,y,w,h) in faces:
        cv2.rectangle(img, (x,y),(x+w,y+h),(120,56,144),10) 
        if x<100:
            #os.system("left.mp3")
            pygame.mixer.music.load("left.mp3")
            pygame.mixer.music.play()


            
        if x>350:
            pygame.mixer.music.load("right.mp3")
            pygame.mixer.music.play()
            #os.system("right.mp3")
        
        

    cv2.imshow('img',img)
    if  cv2.waitKey(1) & 0xff == ord('q'):
        break 

           
cap.release()
cv2.destroyAllWindows()